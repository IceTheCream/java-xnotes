package org.ice.xnotes;

//import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.io.File;
//import java.awt.event.ActionEvent;
//import java.awt.event.ActionListener;
//import java.io.File;
//import java.io.FileInputStream;
//import java.io.FileNotFoundException;
import java.io.IOException;


//import javax.swing.JButton;
//import javax.swing.JCheckBox;
import javax.swing.JFrame;
//import javax.swing.JPanel;
//import javax.swing.JScrollPane;
//import javax.swing.JTable;
//import javax.swing.JTextField;
import javax.swing.UIManager;
//import javax.swing.border.EmptyBorder;


import org.ice.tlib.config.ConfigException;
import org.ice.tlib.config.EasyConfig;


//import java.util.Properties;
import java.awt.event.WindowAdapter;
//import java.awt.event.ActionListener;
//import java.awt.event.ActionEvent;
import java.awt.event.WindowEvent;
//import java.sql.SQLException;
//import java.util.Properties;


import org.ice.tlib.db.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;



public class MainFrame extends JFrame {


    /**
	 * 
	 */
	private static final long serialVersionUID = 6868465037768983655L;
	//private final String configFile="xnotes.ini";
	private final static String configFolder="~/.xnotes/";
	private final static String settingsFile="settings.ini";
	private final static String configFile="config.ini";
	

	
	private String dbPath=null;
	private static EasyConfig settings = null;
	private DataBase dataBase=null;
	
	/*private EasyConfig config = null;
	private JPanel mainPanel=null;
	private JPanel searchPanel=null;
	private JPanel contentPanel=null;
	private JTable notesTable=null;
	
	private Query query=null;*/
	//private DBStorage storage=null;
	private final String dbDriver="";
	private final String dbUrl ="";
	private final String dbEncoding="";
	private final String dbUser="";
	private final String dbPass="";


	
	private static MainFrame mainFrame=null;
	private static Logger logger=null;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		
		EventQueue.invokeLater(new Runnable() {
			
			public void run() {
				
				try {

					//UIManager.setLookAndFeel("javax.swing.plaf.metal.MetalLookAndFeel")
                    //UIManager.setLookAndFeel(UIManager.getCrossPlatformLookAndFeelClass					
					logger = LoggerFactory.getLogger(MainFrame.class);
					UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
					mainFrame = new MainFrame();
					mainFrame.setVisible(true);
					mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

					if(isConfigFolderExists()) {
						
						//***** Restore frame pos and size
						settings=new EasyConfig(settingsFile); //Путь?
						if(new File(settingsFile).exists()) {
							try {
								settings.loadFrame(mainFrame);
							} catch (IOException | ConfigException e) {
								
								logger.debug("[mainFrame] Ошибка 4001: не удалось загрузить параметры окна."+e.getMessage());
								mainFrame.setBounds(100, 100, 450, 300);
							}
						}
					}
							
					mainFrame.setVisible(true);
					mainFrame.buildInterface();
					logger.debug("Debug!");
					logger.warn("Warning!");
					logger.error("Error!");
					
					//***** Closing the program
					mainFrame.addWindowListener(new WindowAdapter() {

						@Override
						public void windowClosing(WindowEvent arg0) {

							try {
					
								settings.saveFrame(mainFrame);
							} catch (IOException e) {

								logger.error("[mainFrame] Ошибка 4002: Не удалось сохранить параметры окна. "+e.getMessage());
							}
						}
					});
					
				} catch (Exception e) {
					
					logger.error("[mainFrame] Ошибка 4003: Окно не может быть создано. "+e.getMessage());
					//e.printStackTrace();
				}
			}
		});
	}

	
	/**
	 * Create the application.
	 */
	public MainFrame() {
		
	}

	
	/**
	 * Initialize the contents of the frame.
	 */
	private void buildInterface() {
		
		/*
		mainFrame.setBounds(100, 100, 450, 300);
		mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		mainFrame.setTitle("xNotes ver. 0.0.1 '14 ");
		mainFrame.setVisible(true);
        
		//***** �������� ������
		mainPanel = new JPanel();
		mainPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		mainPanel.setLayout(new BorderLayout());
		mainFrame.setContentPane(mainPanel);
        
		//***** ������ ������
		searchPanel = new JPanel();
		searchPanel.setLayout(new BorderLayout());
		
		//***** ������ ����� ��������� ��� ������
		JTextField searchLine = new JTextField();
		searchPanel.add(searchLine,BorderLayout.CENTER);

		//***** ������ ������� ������
        JButton searchButton = new JButton("������");
        searchButton.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent arg0) {
                	// ��� ����� �����
                }
        });
        searchPanel.add(searchButton,BorderLayout.LINE_END);
        
        //***** ������ ������ �� �����������
        JCheckBox contentCheckBox = new JCheckBox("������ � ����������");
        searchPanel.add(contentCheckBox,BorderLayout.PAGE_END);
       
        mainPanel.add(searchPanel,BorderLayout.PAGE_START);

        //JList contentList = new JList(null);
        //contentPanel = new JPanel();
		JScrollPane scrollPane = new JScrollPane();
		mainPanel.add(scrollPane, BorderLayout.CENTER);
		
		DataTableModel model=new DataTableModel(storage);
		notesTable = new JTable(model);
		//masterTable = new EasyTable(model);
		
		scrollPane.setViewportView(notesTable);
		*/
    
	
        
		/*
                JMenuBar menuBar = new JMenuBar();
                setJMenuBar(menuBar);

                contentPane = new JPanel();
                contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
                setContentPane(contentPane);
                contentPane.setLayout(new BorderLayout(0, 0));

                JLayeredPane layeredPane = new JLayeredPane();
                contentPane.add(layeredPane);
                layeredPane.setLayout(new BorderLayout(0, 0));
                
                JToolBar toolBar = new JToolBar();
                layeredPane.setLayer(toolBar, 0);
                toolBar.setRollover(true);
                layeredPane.add(toolBar, BorderLayout.NORTH);

                JButton buttonAdd = new JButton("\u0414\u043E\u0431\u0430\u0432\u0438\u0442\u044C   ");
                buttonAdd.addActionListener(new ActionListener() {
                        public void actionPerformed(ActionEvent arg0) {
                        }
                });
                
                 buttonAdd.setIcon(new ImageIcon(MainFrame.class.getResource("/IMG/list-add.png")));
                toolBar.add(buttonAdd);

                JButton buttonEdit = new JButton("\u041F\u0440\u0430\u0432\u0438\u0442\u044C   ");
                buttonEdit.addActionListener(new ActionListener() {
                        public void actionPerformed(ActionEvent e) {
                        }
                });
                buttonEdit.setIcon(new ImageIcon(MainFrame.class.getResource("/IMG/accessories-text-editor.pn
                toolBar.add(buttonEdit);

                JButton buttonDelete = new JButton("\u0423\u0434\u0430\u043B\u0438\u0442\u044C   ");
                buttonDelete.addActionListener(new ActionListener() {
                        public void actionPerformed(ActionEvent e) {
                        }
                });
                buttonDelete.setIcon(new ImageIcon(MainFrame.class.getResource("/IMG/list-remove.png")));
                toolBar.add(buttonDelete);
                
                JButton buttonExit = new JButton("\u0412\u044B\u0439\u0442\u0438   ");
                buttonExit.addActionListener(new ActionListener() {
                        public void actionPerformed(ActionEvent e) {
                                dataBase.close();
                                try {
                                        masterQuery.close();
                                } catch (SQLException e1) {
                                        System.out.println("\n *** Err. MainFrame:Exiting:masterQuery.close -
                                } catch (DataBaseException e1) {
                                        System.out.println("\n *** Err. MainFrame:Exiting:masterQuery.close -
                                }
                                System.exit(NORMAL);
                        }
                });
                        
                JButton buttonDopik = new JButton("\u0414\u043E\u043F\u0438\u043A   ");
                buttonDopik.setIcon(new ImageIcon(MainFrame.class.getResource("/IMG/folder.png")));
                toolBar.add(buttonDopik);

                    JButton buttonRefs = new JButton("");
                buttonRefs.addActionListener(new ActionListener() {
                        public void actionPerformed(ActionEvent arg0) {
                                DirectorySelector selector=new DirectorySelector(dataBase);
                                selector.setVisible(true);
                        }
                });
                buttonRefs.setIcon(new ImageIcon(MainFrame.class.getResource("/IMG/x-office-address-book.png"
                toolBar.add(buttonRefs);
                buttonExit.setIcon(new ImageIcon(MainFrame.class.getResource("/IMG/application-exit.png")));
                toolBar.add(buttonExit);


                JSeparator separator = new JSeparator();
                separator.setOrientation(SwingConstants.VERTICAL);
                toolBar.add(separator);

                //@SuppressWarnings(UNCHECKED2)
                comboBox = new JComboBox<String>();
                for(int i=0;i<yearsFilter.length;i++) comboBox.addItem(yearsFilter[i]);
                comboBox.setEditable(false);
                toolBar.add(comboBox);
                
 
                JButton buttonApplyYear = new JButton("");
                buttonApplyYear.addActionListener(new ActionListener() {
                        public void actionPerformed(ActionEvent arg0) {
                                currentYear=(String) comboBox.getItemAt(comboBox.getSelectedIndex());
                            dbReOpen();
                        }
                });
                buttonApplyYear.setIcon(new ImageIcon(MainFrame.class.getResource("/IMG/dialog-ok.png")));
                toolBar.add(buttonApplyYear);

                JScrollPane scrollPane = new JScrollPane();
                layeredPane.add(scrollPane, BorderLayout.CENTER);

                DataTableModel model=new DataTableModel(masterStorage);
                masterTable = new JTable(model);
                //masterTable = new EasyTable(model);

                scrollPane.setViewportView(masterTable);                                        
                             
		 */
	
	}
	
	
	//***** Connect to database
	public int dbConnect() {
		
		dataBase=new DataBase();
		dataBase.setDriver(dbDriver);
		dataBase.setDBUrl(dbUrl);
		dataBase.setDBPath(dbPath);
		dataBase.setEncoding(dbEncoding);
		dataBase.setUser(dbUser);
		dataBase.setPassword(dbPass);
		dataBase.open();
		/*
		query=new Query(dataBase.getConnection());
		//query.addParameter("pyear", "2009");
		//masterQuery.addParameter("porderfield", "M.fcontractnumber");
		storage=new DBStorage();
		storage.setQuery(query);
		storage.addFieldDef(new FieldDef("FYEAR","���",FieldDef.FieldTypes.ftINT,true));
		storage.addFieldDef(new FieldDef("FCONTRACTNUMBER","N ���������",FieldDef.FieldTypes.ftSTRING,true));
		storage.addFieldDef(new FieldDef("ATYPENAME","��� ��������",FieldDef.FieldTypes.ftSTRING,true));
		storage.addFieldDef(new FieldDef("AORGNAME","�����������",FieldDef.FieldTypes.ftSTRING,true));
		storage.addFieldDef(new FieldDef("ASUBJNAME","�������",FieldDef.FieldTypes.ftSTRING,true));
		storage.addFieldDef(new FieldDef("FNOTICE","����������",FieldDef.FieldTypes.ftSTRING,true));
		storage.addFieldDef(new FieldDef("FREGISTRATIONDATE","���� �����������",FieldDef.FieldTypes.ftDATE,true));
		storage.addFieldDef(new FieldDef("FRECORDDATE","���� ������",FieldDef.FieldTypes.ftDATE,true));
		storage.setKeyFieldIndex(0); 
		
		try {
			query.open("");
			storage.loadData();
		} catch (SQLException e) {
			System.out.println("\n *** Err. MainFrame:dbConnect:Qr.open - Exception occured. "+e.getMessage());
			return 0;
		} catch (DataBaseException e) {
			System.out.println("\n *** Err. MainFrame:dbConnect:Qr.open - Exception occured. "+e.getMessage());
			return 0;
		}*/
		return 1;
	}

	
	private static boolean isConfigFolderExists() {
		
		File fl=new File(configFolder);
		if(fl.exists()) {
		
			// Папка существует. Можно ли в нее писать?
			if(fl.canWrite()) {
				
				// Все в порядке. Можно ли читать оттуда?
				if(fl.canRead()) {
					
					return true;
				} else {
					
					// Ай-я-яй! Папка недоступна для чтения!
					logger.error("[mainFrame] Ошибка 2102: Фатальная ошибка - папка ~/.xnotes недоступна для чтения! ");
					return false;
				}
			} else {
				
				// Ай-я-яй! В папку нельзя писать!
				logger.error("[mainFrame] Ошибка 2101: Фатальная ошибка - папка ~/.xnotes недоступна для записи! ");
				return false;
			}
		} else {
			
			//создать папку.
			if(fl.mkdir()) {
				
				return true;
			} else {
				
				// Ай-я-яй! Нельзя создать папку!
				logger.error("[mainFrame] Ошибка 2103: Фатальная ошибка - папка ~/.xnotes недоступна для записи! ");
				return false;
			}
		}
	}
}
	


